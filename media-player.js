document.addEventListener("DOMContentLoaded", function() { initialiseMediaPlayer(); }, false);

// Variables to store handles to various required elements
var mediaPlayer;
var playPauseBtn;
var muteBtn;
var progressBar;

// Remember the current video playing for touch events
var currentVideoNumber = 1;

// Only add 1 event listener for changing videos
var loadVideoEventListener = false;

// Use an indexed list to avoid duplicating names everywhere
var videoList = [
    "http://x-default-stgec.uplynk.com/ausw/slices/c19/fbf7760291f14e3d95dd0d6f4c2be7e6/c19003c7fa4440c3bdac668ccc78b87e/c19003c7fa4440c3bdac668ccc78b87e_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/6b4/fbf7760291f14e3d95dd0d6f4c2be7e6/6b4e615d9d864ca197528dfead9277e9/6b4e615d9d864ca197528dfead9277e9_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/ded/fbf7760291f14e3d95dd0d6f4c2be7e6/ded0e823394348fab41f77fc9492aa91/ded0e823394348fab41f77fc9492aa91_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/ecc/fbf7760291f14e3d95dd0d6f4c2be7e6/ecc9fb2e6e69436eb75b0dd63d02b0ae/ecc9fb2e6e69436eb75b0dd63d02b0ae_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/f9c/fbf7760291f14e3d95dd0d6f4c2be7e6/f9c29a7551e348498d554846615d40c7/f9c29a7551e348498d554846615d40c7_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/f88/fbf7760291f14e3d95dd0d6f4c2be7e6/f88610c010af48c285d2b1393eb7731c/f88610c010af48c285d2b1393eb7731c_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/c30/fbf7760291f14e3d95dd0d6f4c2be7e6/c30c2d6c8265476b803a9462ad2e584d/c30c2d6c8265476b803a9462ad2e584d_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/de6/fbf7760291f14e3d95dd0d6f4c2be7e6/de62df89d319421c9af2a04ad5938272/de62df89d319421c9af2a04ad5938272_g.mp4",
    "http://x-default-stgec.uplynk.com/ausw/slices/1cb/fbf7760291f14e3d95dd0d6f4c2be7e6/1cb32d7e8b1e4d78b4d9db2c3d0bfc5f/1cb32d7e8b1e4d78b4d9db2c3d0bfc5f_g.mp4"
];

// Browser identification
var client = getClient();
console.log(JSON.stringify({ browser:client.browser,engine:client.engine,system:client.system},null, 2));

function initialiseMediaPlayer() {
    // Get a handle to the player
    mediaPlayer = document.getElementById('media-video');

    // Get handles to each of the buttons and required elements
    playPauseBtn = document.getElementById('play-pause-button');
    muteBtn = document.getElementById('mute-button');
    progressBar = document.getElementById('progress-bar');

    // Hide the browser's default controls
    mediaPlayer.controls = false;

    // Add a listener for the timeupdate event so we can update the progress bar
    mediaPlayer.addEventListener('timeupdate', updateProgressBar, false);

    // Add a listener for the play and pause events so the buttons state can be updated
    mediaPlayer.addEventListener('play', function() {
        // Change the button to be a pause button
        changeButtonType(playPauseBtn, 'pause');
    }, false);
    mediaPlayer.addEventListener('pause', function() {
        // Change the button to be a play button
        changeButtonType(playPauseBtn, 'play');
    }, false);

    // need to work on this one more...how to know it's muted?
    mediaPlayer.addEventListener('volumechange', function(e) {
        // Update the button to be mute/unmute
        if (mediaPlayer.muted) changeButtonType(muteBtn, 'unmute');
        else changeButtonType(muteBtn, 'mute');
    }, false);

    mediaPlayer.addEventListener('ended', function() { this.pause(); }, false);

    mediaPlayer.addEventListener('fullscreenchange', function() {
        console.log("Recieved fullscreenchange event")
    }, false);

    mediaPlayer.addEventListener('fullscreenerror', function() {
        console.log("Recieved fullscreenerror event")
    }, false);

    mediaPlayer.addEventListener('load', function() {
        console.log("Load event listener");
        var elem = document.getElementById('videoTouch');
        swipeDetect(elem, function(swipedir) {
            console.log("SwipeDetect function(" + swipedir + ")");
            switch (swipedir) {
                default:
                case 'none':
                case 'up':
                case 'down':
                    return;
                    break;
                case 'left':
                    currentVideoNumber = (currentVideoNumber +1 + videoList.length) % videoList.length;
                    mediaPlayer.loadVideo(videoList[((currentVideoNumber-1+videoList.length)%videoList.length)]);
                    break;
                case 'right':
                    currentVideoNumber = (currentVideoNumber -1 + videoList.length) % videoList.length;
                    mediaPlayer.loadVideo(videoList[((currentVideoNumber-1+videoList.length)%videoList.length)]);
                    break;
            }

        });
    }, false);

}

function togglePlayPause() {
    // If the mediaPlayer is currently paused or has ended
    if (mediaPlayer.paused || mediaPlayer.ended) {
        // Change the button to be a pause button
        changeButtonType(playPauseBtn, 'pause');
        // Play the media
        mediaPlayer.play();
    }
    // Otherwise it must currently be playing
    else {
        // Change the button to be a play button
        changeButtonType(playPauseBtn, 'play');
        // Pause the media
        mediaPlayer.pause();
    }
}

// Stop the current media from playing, and return it to the start position
function stopPlayer() {
    mediaPlayer.pause();
    mediaPlayer.currentTime = 0;
}

// Changes the volume on the media player
function changeVolume(direction) {
    if (direction === '+') mediaPlayer.volume += mediaPlayer.volume == 1 ? 0 : 0.1;
    else mediaPlayer.volume -= (mediaPlayer.volume == 0 ? 0 : 0.1);
    mediaPlayer.volume = parseFloat(mediaPlayer.volume).toFixed(1);
}

// Toggles the media player's mute and unmute status
function toggleMute() {
    if (mediaPlayer.muted) {
        // Change the cutton to be a mute button
        changeButtonType(muteBtn, 'mute');
        // Unmute the media player
        mediaPlayer.muted = false;
    }
    else {
        // Change the button to be an unmute button
        changeButtonType(muteBtn, 'unmute');
        // Mute the media player
        mediaPlayer.muted = true;
    }
}

// Replays the media currently loaded in the player
function replayMedia() {
    resetPlayer();
    mediaPlayer.play();
}

function fullScreen() {
    if (!document.fullscreenElement) {
        /* Firefox: must enable full-screen-api.unprefix.enabled in about:config,
           then use the unprefixed name */
        if (mediaPlayer.requestFullscreen) {
            mediaPlayer.requestFullscreen();
            console.log("requestFullscreen executed");
        } else if (mediaPlayer.webkitRequestFullscreen) {
            mediaPlayer.webkitRequestFullscreen();
            console.log("webkitRequestFullscreen executed");
        } else if (mediaPlayer.mozRequestFullscreen) {
            mediaPlayer.mozRequestFullscreen();
            console.log("mozRequestFullscreen executed");
        } else if (mediaPlayer.msRequestFullscreen) {
            mediaPlayer.msRequestFullscreen();
            console.log("msRequestFullscreen executed");
        } else {
            console.log("Could not find a requestFullscreen variant");
        }
    } else {
        if (mediaPlayer.exitFullscreen) {
            mediaPlayer.exitFullscreen();
            console.log("exitFullscreen executed");
        } else if (mediaPlayer.webkitExitFullscreen) {
            mediaPlayer.webkitExitFullscreen();
            console.log("webkitExitFullscreen executed");
        } else if (mediaPlayer.mozCancelFullscreen) {
            mediaPlayer.mozCancelFullscreen();
            console.log("mozExitFullscreen executed");
        } else if (mediaPlayer.msExitFullscreen) {
            mediaPlayer.msExitFullscreen();
            console.log("msExitFullscreen executed");
        } else {
            console.log("Could not find an exitFullscreen variant");
        }
    }
}

// Update the progress bar
function updateProgressBar() {
    //Work out how much of the media has played via the duration and currentTime parameters

    if (!(isNaN(mediaPlayer.currentTime) || isNaN(mediaPlayer.duration))) {
        var percentage = Math.floor((100 / mediaPlayer.duration) * mediaPlayer.currentTime);
        // Update the progress bar's value
        // console.log("updateProgressBar = " + percentage)
        progressBar.value = percentage;
        // Update the progress bar's text (for browsers that don't support the progress element)
        progressBar.innerHTML = percentage + '% played';
    } else {
        console.log("The progress bar cannot be computed.");
        console.log("mediaPlayer.currentTime = " + mediaPlayer.currentTime);
        console.log("mediaPlayer.duration = " + mediaPlayer.duration);
        progressBar.value = 0;
        // Update the progress bar's text (for browsers that don't support the progress element)
        progressBar.innerHTML = 'NaN% played';
    }
}

// Updates a button's title, innerHTML and CSS class to a certain value
function changeButtonType(btn, value) {
    btn.title = value;
    btn.innerHTML = value;
    btn.className = value;
}

// Handle keypress events by loading the corresponding video, and updating the current video number
document.addEventListener('keypress', function(e) {
    var k = (e || window.event).key
    console.log("The keypress handler saw key=(" + k + ")");
    var kk = k.charCodeAt(0) - '0'.charCodeAt(0);
    if (kk in videoList) {
        console.log("Loading video (" + k + "): " + videoList[kk]);
        loadVideo(videoList[kk]);
        currentVideoNumber = k;
    } else {
        console.log("(" + k + "), number (" + kk + "), is outside of video list range of (" + videoList.length + ")");
    }},
    false)


// Loads a video item into the media player
function loadVideo() {
    for (var i = 0; i < arguments.length; i++) {
        mediaPlayer.pause();
        var previousTime = mediaPlayer.currentTime;
        console.log("Captured previousTime is " + previousTime);
        var previousDuration = mediaPlayer.duration;
        console.log("Captured duration is " + previousDuration);
        var file = arguments[i].split('.');
        var ext = file[file.length - 1];
        // Check if this media can be played
        if (canPlayVideo(ext)) {
            // Reset the player, change the source file and load it
            resetPlayer();
            mediaPlayer.src = arguments[i];
                console.log("This is the mediaPlayer.currentTime after the NEW SOURCE " +mediaPlayer.currentTime);
            mediaPlayer.load();
                console.log("This is the mediaPlayer.currentTime after the LOAD " +mediaPlayer.currentTime);
            mediaPlayer.currentTime = previousDuration;
                console.log("This is the mediaPlayer.currentTime after assignment to duration " + mediaPlayer.currentTime);
            // Chrome works with canplay and loadedmetadata
            if (client.engine.webkit && !client.browser.chrome) {
                mediaPlayer.addEventListener("canplay", function () {
                        console.log("canplay event triggered");
                        console.log("This is the mediaPlayer.currentTime after canplay triggered " + mediaPlayer.currentTime);
                        console.log("callback previousTime is " + previousTime);
                    mediaPlayer.currentTime = previousTime;
                        console.log("This is the mediaPlayer.currentTime after assignment to previousTime " + mediaPlayer.currentTime);
                    mediaPlayer.play();
                        console.log("Play after canplay triggered");
                        console.log("This is the mediaPlayer.currentTime after canplay triggered play " + mediaPlayer.currentTime);
                }, false);
            } else { // Firefox doesn't work with canplay, needs loadedmetadata
                mediaPlayer.addEventListener("loadedmetadata", previousTimeCallback(mediaPlayer, "loadedmetadata", previousTime), false);
            }
            break;
        }
    }
}

function previousTimeCallback(mediaPlayer, eventKind, previousTime) {
        console.log(eventKind + " event triggered");
        console.log("This is the mediaPlayer.currentTime after " + eventKind + " triggered " + mediaPlayer.currentTime);
        console.log("callback previousTime is " + previousTime);
    mediaPlayer.currentTime = previousTime;
        console.log("This is the mediaPlayer.currentTime after assignment to previousTime " + mediaPlayer.currentTime);
    mediaPlayer.play();
        console.log("Play after " + eventKind + " triggered");
        console.log("This is the mediaPlayer.currentTime after " + eventKind + " triggered play " + mediaPlayer.currentTime);
    // remove this callback
    mediaPlayer.removeEventListener(eventKind, previousTimeCallback, false);
}

// Checks if the browser can play this particular type of file or not
function canPlayVideo(ext) {
    var ableToPlay = mediaPlayer.canPlayType('video/' + ext);
    if (ableToPlay == '') return false;
    else return true;
}

// Resets the media player
function resetPlayer() {
    // Reset the progress bar to 0
    progressBar.value = 0;
    // Move the media back to the start
    mediaPlayer.currentTime = 0;
    // Ensure that the play pause button is set as 'play'
    changeButtonType(playPauseBtn, 'play');
}

function swipeDetect(elem, callback){

    var touchsurface = elem,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 150, //required min distance traveled to be considered swipe
    restraint = 100, // maximum distance allowed at the same time in perpendicular direction
    allowedTime = 300, // maximum time allowed to travel that distance
    elapsedTime,
    startTime,
    handleswipe = callback || function(swipedir){}

    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
        e.preventDefault()
    }, false)

    touchsurface.addEventListener('touchmove', function(e){
        e.preventDefault() // prevent scrolling when inside DIV
    }, false)

    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        if (elapsedTime <= allowedTime){ // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
                swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }
        handleswipe(swipedir)
        e.preventDefault()
    }, false)
}

function getClient(){

  //rendering engines
  var engine = {
    ie: 0,
    edge: 0,
    gecko: 0,
    webkit: 0,
    khtml: 0,
    opera: 0,

    //complete version
    ver: null
  };

  //browsers
  var browser = {

    //browsers
    ie: 0,
    edge: 0,
    firefox: 0,
    safari: 0,
    konq: 0,
    opera: 0,
    chrome: 0,
    safari: 0,

    //specific version
    ver: null
  };


  //platform/device/OS
  var system = {
    win: false,
    mac: false,
    x11: false,

    //mobile devices
    iphone: false,
    ipod: false,
    nokiaN: false,
    winMobile: false,
    macMobile: false,

    //game systems
    wii: false,
    ps: false
  };

  //detect rendering engines/browsers
  var ua = navigator.userAgent;
  if (window.opera) {
    engine.ver = browser.ver = window.opera.version();
    engine.opera = browser.opera = parseFloat(engine.ver);
  } else if (/Edge\/([^;]+)/.test(ua)) { // IE11
    engine.ver = browser.ver = RegExp["$1"];
    engine.edge = browser.edge = parseFloat(engine.ver);
  } else if (/AppleWebKit\/(\S+)/.test(ua)) {
    engine.ver = RegExp["$1"];
    engine.webkit = parseFloat(engine.ver);

    //figure out if it's Chrome or Safari
    if (/Chrome\/(\S+)/.test(ua)) {
      browser.ver = RegExp["$1"];
      browser.chrome = parseFloat(browser.ver);
    } else if (/Version\/(\S+)/.test(ua)) {
      browser.ver = RegExp["$1"];
      browser.safari = parseFloat(browser.ver);
    } else {
      //approximate version
      var safariVersion = 1;
      if (engine.webkit < 100) {
        safariVersion = 1;
      } else if (engine.webkit < 312) {
        safariVersion = 1.2;
      } else if (engine.webkit < 412) {
        safariVersion = 1.3;
      } else {
        safariVersion = 2;
      }

      browser.safari = browser.ver = safariVersion;
    }
  } else if (/KHTML\/(\S+)/.test(ua) || /Konqueror\/([^;]+)/.test(ua)) {
    engine.ver = browser.ver = RegExp["$1"];
    engine.khtml = browser.konq = parseFloat(engine.ver);
  } else if (/rv:([^\)]+)\) Gecko\/\d{8}/.test(ua)) {
    engine.ver = RegExp["$1"];
    engine.gecko = parseFloat(engine.ver);

    //determine if it's Firefox
    if (/Firefox\/(\S+)/.test(ua)) {
      browser.ver = RegExp["$1"];
      browser.firefox = parseFloat(browser.ver);
    }
  } else if (/MSIE ([^;]+)/.test(ua)) { // IE <= 10
    engine.ver = browser.ver = RegExp["$1"];
    engine.ie = browser.ie = parseFloat(engine.ver);
  } else if (/Trident\/([^;]+)/.test(ua)) { // IE11
    engine.ver = RegExp["$1"];
    browser.ver = parseFloat(ua.split("rv:")[1]);
    engine.ie = parseFloat(browser.ver);
  }

  //detect browsers
  browser.ie = engine.ie;
  browser.opera = engine.opera;


  //detect platform
  var p = navigator.platform;
  system.win = p.indexOf("Win") == 0;
  system.mac = p.indexOf("Mac") == 0;
  system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);

  //detect windows operating systems
  if (system.win) {
    if (/Win(?:dows )?([^do]{2})\s?(\d+\.\d+)?/.test(ua)) {
      if (RegExp["$1"] == "NT") {
        switch (RegExp["$2"]) {
          case "5.0":
            system.win = "2000";
            break;
          case "5.1":
            system.win = "XP";
            break;
          case "6.0":
            system.win = "Vista";
            break;
          default:
            system.win = "NT";
            break;
        }
      } else if (RegExp["$1"] == "9x") {
        system.win = "ME";
      } else {
        system.win = RegExp["$1"];
      }
    }
  }

  //mobile devices
  system.iphone = ua.indexOf("iPhone") > -1;
  system.ipod = ua.indexOf("iPod") > -1;
  system.nokiaN = ua.indexOf("NokiaN") > -1;
  system.winMobile = (system.win == "CE");
  system.macMobile = (system.iphone || system.ipod);

  //gaming systems
  system.wii = ua.indexOf("Wii") > -1;
  system.ps = /playstation/i.test(ua);

  //return it
  return {
    engine: engine,
    browser: browser,
    system: system
  };
}


